import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemsdbService } from '../itemsdb.service';
import { ShoppingItem } from '../../shoppingitem';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  itemCount: number;
  items = Array<ShoppingItem>();
  newItem: string;
  newItemQuantity: number;
  @ViewChild('myInput', null) myInput;

  constructor(private itemsDB: ItemsdbService) { }

  ngOnInit() {
    this.itemsDB.getAll()
      .then(data => {
        this.items = JSON.parse(String(data));
        if (!this.items) {
          this.items = [];
        }
        this.itemCount = this.items.length;
      }, err => {
        console.log(err);
      });
  }

  addItem(event) {
    if (event.keyCode === 13) {
      const newShoppingItem: ShoppingItem = new ShoppingItem();
      newShoppingItem.name = this.newItem;
      newShoppingItem.quantity = this.newItemQuantity;
      this.items.push(newShoppingItem);
      this.newItem = '';
      this.myInput.setFocus();
      this.itemCount = this.items.length;
      this.itemsDB.save(this.items);
    }
  }

  removeItem(index) {
    this.items.splice(index, 1);
    this.itemsDB.save(this.items);
  }

}
